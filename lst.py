import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Pango

from timeit import default_timer

from math import floor, log

from decimal import Decimal

# VARIABLES

app_on = True

x = None

timer_onoff = False

time_to_put = ""

cut_num = 5

timer_font = Pango.FontDescription("monospace heavy")

seconds = 0

minutes = 0

hours = 0

# GTK

class Window(Gtk.Window):

    def __init__(self):

        global x
        global timer_font
        global list_of_splits

        Gtk.Window.__init__(self, title="Linux Speedrun Timer")
        self.set_border_width(10)

        vbox = Gtk.Box(orientation = Gtk.Orientation.VERTICAL, spacing=10)
        self.add(vbox)
        hbox1 = Gtk.Box(spacing=6)
        hbox2 = Gtk.Box(spacing=6)
        hbox3 = Gtk.Box(spacing=6)

        hboxs = Gtk.Box(spacing=6)

        vbox.pack_start(hbox1, True, True, 0)
        vbox.pack_start(hbox2, True, True, 0)
        vbox.pack_start(hboxs, True, True, 0)
        vbox.pack_start(hbox3, True, True, 0)

        toggle = Gtk.ToggleButton(label = "Timer ON")
        toggle.connect("toggled", self.timerbutton)
        hbox3.pack_start(toggle, True, True, 0)

        button = Gtk.Button.new_with_label("Split")
        button.connect("clicked", self.split)
        hbox3.pack_start(button, True, True, 0)

        button = Gtk.Button.new_with_label("Exit")
        button.connect("clicked", self.exit)
        hbox3.pack_start(button, True, True, 0)

        label = Gtk.Label()
        label.set_markup("<b><big>Linux Speedrun Timer</big></b>")
        hbox1.pack_start(label, True, True, 0)

        labelx = Gtk.Label()
        labelx.set_markup("00:00:00:00")
        labelx.modify_font(timer_font)
        hbox2.pack_start(labelx, True, True, 0)

        x = labelx

        list_of_splits = Gtk.ComboBoxText()
        list_of_splits.append_text("Splits")
        hboxs.pack_start(list_of_splits, True, True, 0)

        self.connect("key-press-event", self.on_key_press)

    def on_key_press(self, key, event):
        print("key " + str(event.keyval) + " pressed")
        if event.keyval == 65438:
            self.split()

    def timerbutton(self, widget):
        global timer_onoff
        global start

        if timer_onoff == False:
            start = default_timer()
            widget.set_label("Timer OFF")
            timer_onoff = True
            print("timer has been turned on")
        else:
            widget.set_label("Timer ON")
            timer_onoff = False
            print("timer has been turned off")

    def split(self):

        global list_of_splits

        print("time has been splitted")

        list_of_splits.append_text(str(time_to_put))

    def exit(self, widget):
        global app_on

        print("application has been closed")
        app_on = False

    def update(self):
        global x

        global time_to_put

        global cut_num

        global minutes

        global hours

        global seconds

        time_to_put = ""

        #time_to_put = str(duration)[:-14]

        cut_num = time_to_put.find(".")

        #if len(time_to_put) - cut_num > 3:
        #    if len(time_to_put) - cut_num == 4:
        #        time_to_put = time_to_put[:-1]
        #    elif len(time_to_put) - cut_num == 5:
        #        time_to_put = time_to_put[:-2]
        #if len(time_to_put) - cut_num < 2:
        #    time_to_put = time_to_put + "0"

        #if int(str(floor(log(duration)/log(60)))) > 0:
        #    minutes = floor(log(duration)/log(60))

        seconds = duration-(hours*60+minutes*60)

        if seconds < 10:
            seconds = "0" + str(seconds)
        else:
            seconds = str(seconds)

        print(len(seconds))

        while len(seconds) > 5:
            seconds = seconds[:-1]

        time_to_put = seconds

        seconds = int(Decimal(seconds))

        if minutes == 60:
            minutes = 0
            hours += 1

        if seconds > 59:
            seconds -= 60
            minutes += 1

        if minutes >= 0:
            if minutes < 10:
                time_to_put = "0" + str(minutes) + ":" + time_to_put
            else:
                time_to_put = str(minutes) + ":" + time_to_put

        if hours >= 0:
            if hours < 10:
                time_to_put = "0" + str(hours) + ":" + time_to_put
            else:
                time_to_put = str(hours) + ":" + time_to_put

        x.set_label(time_to_put)


win = Window()
win.connect("destroy", exit)
win.show_all()

# MAIN LOOP

while app_on:

    if timer_onoff:
        duration = default_timer() - start
        win.update()

    Gtk.main_iteration()
